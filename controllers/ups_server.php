<?php

class Ups_server extends ClearOS_Controller
{
    /**
     * Ups Server default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('ups_server');
        
        $views = array(
            
            'ups_server/nut_conf/summary',
            'ups_server/ups_conf/summary_view',
            'ups_server/upsd_users/users',
            'ups_server/upsmon_conf/summary'
        );

        // Load views
        //-----------
        $this->page->view_forms($views,'ups_server',lang('ups_server_app_name'));
    }
}
