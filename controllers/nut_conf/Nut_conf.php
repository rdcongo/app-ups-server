<?php

/**
 * Ups Server controller.
 *
 * @category   Apps
 * @package    Ups_Server
 * @subpackage Views
 * @author     Your name <your@e-mail>
 * @copyright  2018 Your name / Company
 * @license    Your license
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Ups Server controller.
 *
 * @category   Apps
 * @package    Ups_Server
 * @subpackage Controllers
 * @author     Your name <scottavalone@gmail.com>
 * @copyright  2018 / ITOT Company
 * @license    Your license
 */

class Nut_conf extends ClearOS_Controller
{
    // Upload the Views

    function index()
    {
        $this->_form('view');
    }
    function edit()
    {
        $this->_form('edit');
    }
    function view()
    {
        $this->_form('view');
    }
    
    // Traitement of functions

    function _form($form_type)
    {
        $this->lang->load('ups_server');
        $this->load->library('ups_server/nut');

        //Fix Form validation
        $this->form_validation->set_policy('server_mode','ups_server/nut','validate_param',TRUE);
        $form_ok = $this->form_validation->run();
        $form_ok = TRUE;

        if($this->input->post('submit') && ($form_ok === TRUE))
        {
            try
            {
                $this->nut->set_nut_conf($this->input->post('mode'), 'MODE', FALSE);
                $this->nut->set_nut_conf($this->input->post('upsd'), 'UPSD_OPTIONS', TRUE);
                $this->nut->set_nut_conf($this->input->post('upsmon'), 'UPSMON_OPTIONS', TRUE);
                $this->nut->set_nut_conf($this->input->post('poweroff_wait'), 'POWEROFF_WAIT', FALSE);
                $this->page->set_status_updated();

            }catch (Exception $e)
            {
                $this->page->view_exception($e);
                return;
            }
        }
        try
        {
        //==============================================================>    
            $data['form_type'] = $form_type;
            $data['mode'] = $this->nut->get_nut_conf('MODE',FALSE);
        //================================================================>

            // FIX : AJAX   hide options based on mode
            //===========================================>
            $data['show_options'] = TRUE;
            $data['upsd'] = $this->nut->get_nut_conf('UPSD_OPTIONS',TRUE);
            $data['upsmon'] = $this->nut->get_nut_conf('UPSMON_OPTIONS',TRUE);
            $data['poweroff_wait'] = $this->nut->get_nut_conf('POWER_WAIT',FALSE);
            //$data['upsd'] = $this->nut->get_nut_conf('UPSD_OPTION',FALSE);
        }catch (Exception $e)
        {
            $this->page->view_exception($e);
            return;
        }
        $this->page->view_form('ups_server/nut/summary',$data, lang('base_settings'));
    }
}