<?php

$lang['ups_server_app_name'] = 'Ups Server';
$lang['ups_server_app_description'] = 'Ups Server - a description goes here.';

$lang['ups_server_command'] = 'Command';
$lang['ups_server_command_list'] = 'Command List';
$lang['ups_server_directives'] = 'Directives';
$lang['ups_server_description'] = 'Description';
$lang['ups_server_default'] = 'Default';
$lang['ups_server_interface_list'] = 'Interface List';
$lang['ups_server_override'] = 'Override';
$lang['ups_server_server_mode'] = 'Mode';
$lang['ups_server_server_interface'] = 'Server Interface';
$lang['ups_server_server_name'] = 'Server Name';
$lang['ups_server_server_port'] = 'Server Port';
$lang['ups_server_server_list'] = 'Server List';
$lang['ups_server_server_settings'] = 'Server Settings';
$lang['ups_server_support'] = 'Support';
$lang['ups_server_supported'] = 'Supported';
$lang['ups_server_ups_list'] = 'UPS List';
$lang['ups_server_ups_name'] = 'UPS Name';
$lang['ups_server_user_list'] = 'User List';
$lang['ups_server_user_name'] = 'User Name';
$lang['ups_server_variables'] = 'Variables';
