<?php
$headers = array(
    lang('ups_server_command'),
    Enabled,
);

$anchors = array(form_submit_update('submit'),anchor_cancel('/app/ups_server/'));
foreach ($upsd_users_command_list as $id => $details) {
    $detail_buttons = button_set(
        array(
            anchor_custom('/app/content_filter/policy/configure/' . $id, lang('base_configure')),
        )
    );
    $item['title'] = $details['command'];
    $item['action'] = '/app/content_filter/policy/configure/' . $id;
    $item['anchors'] = $detail_buttons;
    $item['details'] = array(
        $details['command'],
        $details['chkd']
    );

    $items[] = $item;
}
echo list_table(
    lang('ups_server_command_list'),
    $anchors,
    $headers,
    $items
);
